Gerando documentação "automática"
=================================

Para a geração da documentação os seguintes passos são recomendados:

1. Abrir o arquivo *conf.py* que está dentro da pasta *source* criado após a execução do Sphinx-quickstart
2. Descomentar as seguintes linhas do código *conf.py*:

.. code-block:: python

	import os
	import sys
	sys.path.insert(0, os.path.abspath('.'))

3. Neste mesmo código, inserir o local do arquivo principal do código no lugar de **' . '**

.. code-block:: python

	sys.path.insert(0, os.path.abspath('C:/Users/pastaX/.../pasta_onde_está_o_código_principal'))

4. Utilizar o sphinx-apidoc para geração automática dos arquivos fontes (.rst) para o Sphinx. Para isso basta executar o seguinte comando no Prompt:

.. code-block:: console

	$ sphinx-apidoc -o outputdir sourcedir

.. note::
	*outputdir* deve ser o local onde serão armazenados os arquivos **.rst** (neste caso recomenda-se armazenar na pasta source criado pelo sphinx-quistart) e o *sourcedir* é o local onde está o código Python principal do programa.

5. Por fim, converte-se para HTML os arquivos fontes gerados no passo anterior utilizando-se o seguinte comando: 

.. code-block:: console

	$ sphinx-build -b html sourcedir outputdir

.. note::
	*sourcedir* é onde estão os arquivos fontes **.rst** (pasta source) e o *outputdir* é a pasta onde serão armazenados os arquivos HTML gerados (pasta *build* criado pelo sphinx-quickstart).


.. tip::
	Criar arquivos batch com todos esses comandos para facilitar o processo.


.. warning:: 
	Este é apenas um tutorial bem básico de como utilizar o Sphinx de forma mais simples (acredito eu) e semi-automática. Esse conjunto **reStructuredText + Sphinx** é bastante flexível e permite o usuário utilizá-lo de diversas formas. Por isso caso tenham interesse, vocês podem se aprofundar mais nas funcionalidades do Sphinx e caso encontrem algo interessante para o nosso projeto, fiquem à vontade para fazer sugestões e caso encontrem problemas ou limitações dessa ferramenta, sintam-se à vontade levar o assunto às nossas reuniões.
Documentação do código utilizando *Docstring*
=============================================

Exemplo de Docstring dentro do código::

	def exemplo(argumento1, argumento2, estado=None):
	    """
	    Esta função faz alguma coisa da seguinte forma: Quisque elit ipsum, porttitor et imperdiet in, 
	    facilisis ac diam. Nunc facilisis interdum felis eget tincidunt. In condimentum fermentum leo, 
	    non consequat leo imperdiet pharetra
	    
	    :param str argumento1: Alguma string. Pellentesque habitant morbi tristique senectus et netus et 
	    	malesuada fames ac turpis egestas.
	    :param int argumento2: Algum número.
	    :param bool estado: Estado atual de algo.

	    :return: Um valor inteiro
	    :rtype: int
	    """

Resultado do HTML desse código:

.. function:: exemplo(argumento1, argumento2, estado=None)

   Esta função faz alguma coisa da seguinte forma: Quisque elit ipsum, porttitor et imperdiet in, facilisis ac diam. Nunc facilisis interdum felis eget tincidunt. In condimentum fermentum leo, non consequat leo imperdiet pharetra

   :param str argumento1: Alguma string. Pellentesque habitant morbi tristique senectus et netus et 
	    malesuada fames ac turpis egestas.
   :param int argumento2: Algum número.
   :param bool estado: Estado atual de algo.

   :return: Um valor inteiro
   :rtype: int


.. note::
	Existem diversas formas de fazer as marcações no código que resultam em uma formatação diferente do HTML. Achei esse formato interessante, mas caso encontrem algum outro, fiquem à vontade para nos sugerir.
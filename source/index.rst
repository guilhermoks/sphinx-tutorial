Tutorial para documentação de código utilizando Sphinx
======================================================

Tutorial básico para a instalação e utilização da ferramenta Sphinx para a documentação de código no projeto VerteSIS.

**Tópicos:**

.. toctree::
   :maxdepth: 1

   Topico1
   Topico2
   Topico3
   Topico4
   Topico5

**Links úteis**

* Site oficial do Sphinx - http://www.sphinx-doc.org/en/master/
* Sintaxe da linguagem de marcação rst - http://docutils.sourceforge.net/docs/user/rst/quickref.html
* Video tutorial 1 - https://www.youtube.com/watch?v=qrcj7sVuvUA
* Vídeo tutorial 2 - https://www.youtube.com/watch?v=LQ6pFgQXQ0Q
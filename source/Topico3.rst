Iniciando a documentação com o Sphinx
=====================================

Criação de diretório para a documentação
----------------------------------------

É possível iniciar a configuração da documentação de duas formas:

Através do PyCharm:
+++++++++++++++++++

Após a instalação do Sphinx a IDE passa a possuir um "atalho"(Sphinx Quickstart) no menu *Tool* para iniciar a configuração da documentação:

.. image:: pycharm_sphinx.png
   :scale: 100 %
   :alt: alternate text
   :align: center

Através do Prompt de comando:
+++++++++++++++++++++++++++++

O recomendado para a utilização dessa opção é executar o seguinte comando dentro do diretório onde estão os códigos a ser documentados: ::

	$ sphinx-quickstart


Configurações do *quickstart*:
++++++++++++++++++++++++++++++

Após a execução do Sphinx quickstart as seguintes opções irão aparecer no Prompt e as seguintes escolhas são recomendadas:

===================================================================================================== ==================================
Prompt                                                         									      Escolha
===================================================================================================== ==================================	
> Root path for the documentation [.]:                         										  <ENTER> 
		
> Separate source and build directories (y/N) [n]:             										  y

> Name prefix for templates and static dir [_]:                										  <ENTER>

> Project name:                                                										  (Colocar nome do projeto)

> Author name(s): 											   										  (Colocar nome do autor)

> Project version: 											   										  (algum número e.g. 0.0.1) 

> Project release [0.0.1]:                                     										  <ENTER>

> Source file suffix [.rst]:                                   										  <ENTER>

> Name of your master document (without suffix) [index]:       										  <ENTER>

> autodoc: automatically insert docstrings from modules (y/N) [n]: 									  y

> doctest: automatically test code snippets in doctest blocks (y/N) [n]:							  n

> intersphinx: link between Sphinx documentation of different projects (y/N) [n]:                     n

> todo: write "todo" entries that can be shown or hidden on build (y/N) [n]:                          n

> coverage: checks for documentation coverage (y/N) [n]:                                              n

> pngmath: include math, rendered as PNG images (y/N) [n]:                                            n

> jsmath: include math, rendered in the browser by JSMath (y/N) [n]:                                  n

> ifconfig: conditional inclusion of content based on config values (y/N) [n]:                        n

> Create Makefile? (Y/n) [y]:                                                                         y

> Create Windows command file? (Y/n) [y]:                                                             y
===================================================================================================== ==================================

Finalizado esse processo as seguintes pastas e arquivos serão criados dentro do local onde estão os códigos:

.. image:: quickstart.PNG
   :scale: 100 %
   :alt: alternate text
   :align: center
Instalando o Sphinx
===================

Visto que o Sphinx é codificado na linguagem Python, é necessário ter instalado o Python (a partir da versão 2.7) para a instalação do Sphinx.

Instalação do Sphinx para Windows
---------------------------------

É possível instalar o Sphinx pelo PyPI através do comando: ::

	$ pip install Sphinx


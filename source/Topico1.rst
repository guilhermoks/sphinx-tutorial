Visão geral
===========

O Sphinx consiste em uma ferramenta que oferece facilidades para a documentação de projetos de software. Foi originalmente criado para documentação Python e utiliza o `Projeto reStructuredText`__ (**.rst** *files*) como linguagem marcada e o converte para vários formatos de saída incluindo HTML, LaTeX, man, EPub e Textinfo.

__ http://docutils.sourceforge.net/rst.html

A documentação pelo Sphinx pode ser feita de duas formas:

**Manual**
----------

Escreve-se manualmente o arquivo **.rst** do código e posteriormente utiliza-se o Sphinx converter esses arquivos para o formato de saída desejado. Portanto nessa opção não se utiliza a marcação em *Docstring* dentro do código uma vez que a documentação é feita diretamente no arquivo **.rst**


**Automático** (modo recomendado para o projeto)
------------------------------------------------

Nessa opção deve-se utilizar primeiramente o *Doctring* (comentários com 3 aspas duplas) com a sintaxe de marcação reStructuredText dentro do código Python para explicar o funcionamento deste. Com isso é possível utilizar uma *API* do Sphinx que importa cada um dos módulos do código, extrai os *docstrings* e os converte para arquivos **.rst**. Posteriormente deve-se converter esses arquivos para o formato de saída desejado gerando assim uma documentação de todo o código.

.. graphviz::
   :align: center

   digraph {
      "Docstring (.py)" -> ".rst" [ label=" sphinx-apidoc" ];
      ".rst" -> "HTML" [ label=" sphinx-build" ];
   }